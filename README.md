# Cha De Bebe

## Sobre este "projeto"

Este projeto explica como montar um site para chá de bebê utilizando Wordpress.

A proposta do site é que haja um espaço para as pessoas convidadas deixarem
recados e também que sirva como "lista de presentes".

Com relação à "lista de presentes", a proposta inicial do site é que a pessoa
que administrar o site cadastre os itens que deseja de forma que as pessoas
convidadas possam escolher os presentes, montar um "carrinho de compra" e, no
final, o pagamento deve ser feito via PIX ou transferência para uma ou mais
contas da família. Com isso a família poderá comprar os itens desejados quando
quiser/precisar. A proposta *não* é que os presentes sejam comprados e enviados
para a casa da família.

## Detalhes técnicos

A implementação do site é feita toda utilizando o *Wordpress* como base e uma
série de plugins (todos sendo usados nas versões gratuitas).

Para além do wordpress, os seguintes plugins foram utilizados:
- [Akismet](https://wordpress.org/plugins/akismet/)
    - Antispam
- [hCaptcha for Forms and More](https://wordpress.org/plugins/hcaptcha-for-forms-and-more/)
    - Captcha simples também para evitar spam
- [Jetpack](https://wordpress.org/plugins/jetpack/)
    - Plugin com uma série de funcionalidades para wordpress, dentre elas gera
        algumas estatísticas e tem algumas features de segurança também.
- [Slider, Gallery, and Carousel by MetaSlider – Responsive WordPress Plugin](https://wordpress.org/plugins/ml-slider/)
    - Usado para gerar o slider de fotos usados na página inicial.
- [One User Avatar](https://wordpress.org/plugins/one-user-avatar/)
    - Usado para configurar um avatar padrão personalizado para quem deixa comentário.
- [WooCommerce](https://wordpress.org/plugins/woocommerce/)
    - Usado para montar a "loja" com os produtos da lista de presentes.
- [Checkout Field Editor for WooCommerce](https://wordpress.org/plugins/woo-checkout-field-editor-pro/)
    - Usado para controlar os campos que aparecem no form do "carrinho".
- [WooCommerce Grid / List toggle](https://wordpress.org/plugins/woocommerce-grid-list-toggle/)
    - Usado para gerar a página de "lista de presentes" com filtros.
- [WP Mail SMTP](https://wordpress.org/plugins/wp-mail-smtp/)
    - Usado para configurar o wordpress para enviar email "pelo gmail".
        Importante para recuperação de senhas e também para notificar quem
        adminsitra o site de "comentários" e "pedidos".

## Configurações do WooCommerce

### Configurações gerais

`Woocommerce` > `Configurações` > `Geral`

- `Locais de Venda`: Vender para todos os países
- `Locais de Entrega`: Desativar entrega e cálculo de frete

### Produto/Inventário

`Woocommerce` > `Configurações` > `Produto` > `Inventário` (está escrito em
letra menor)

Habilitar `Ativar gestão de estoque`.

Também é possível habilitar `Visibilidade dos produtos fora de estoque`.

### Meio de pagamento

`Woocommerce` > `Configurações` > `Pagamentos`

Precisamos criar um "meio de pagamento" "PIX". Para tanto, vamos converter o
meio de pagamento `Pagamento na entrega` em PIX, alterando o título do meio de
pagamento, a descrição, aonde adicionaremos as informações de PIX.

E, claro, habilitar apenas este meio de pagamento.

### Cadastro de produtos

Para produtos mais caros, você pode cadastrá-lo com "cotas". Então, ao invés de
1 produto de R$2.000,00 você pode, por exemplo, cadastrar o produto com 10 cotas
de R$200,00. Isso aumenta as chances de contribuições.

Nós classificamos os produtos em diversas "categorias" (não excludentes).

Para todo produto que nós cadastramos, você precisa cadastrar os seguintes
campos:
- `Nome do produto`
- `Categorias`
- `Imagem do produto`
- `Breve descrição sobre o produto`

E, em "*Dados do Produto*", cadastrar os seguintes campos:
- `Geral`:
    - `Preço`
- `Inventário`:
    - `Ativar gerenciamento de estoque a nível de produto`
    - `Quantidade em estoque`

Se for de interesse, ainda recomendamos:
- `Produtos relacionados`
    - `Aumentar vendas` (cadastrar produtos relacionados, isso ajuda a aumentar
        a quantidade de itens por pedido)

Além disso, vale criamos um atributo especial chamado `Prioridade`, que precisa
de apenas um "valor" (`sim`). Esse atributo deve ser criado em:
https://<seu_site>/wp-admin/edit.php?post_type=product&page=product_attributes

Assim, ao cadastrar um produto novo ainda temos as seguintes configurações a
serem feitas:

- `Atributos`
    - Adicionar um atributo personalizado chamado `site`, com o link
        para a loja que efetivamente tem o produto (apenas quem administra o
        site tem acesso, os visitantes não). Isso ajuda a saber aonde comprar os
        produtos depois.
    - E o atributo `Prioridades` criado, selecionando a opção "`Sim`" para os
        que são prioridade. Dessa forma poderemos ter um filtro de prioridades
        na página de presentes.

### Página de presentes

O Woocommerce, por padrão, cria uma página de "produtos", mas ela não tem
filtros e é pouco configurável. Nós não colocamos essa página em nenhum menu.

O que fizemos foi criar uma outra página "presentes", com os seguintes
elementos:

- Título
- Bloco de texto
- Espaçador
- Separador
- Espaçador
- Bloco com Colunas (2 colunas)
- Na coluna da esquerda adicionar os seguintes blocos:
    - Filtrar produtos por atributo (`Prioritário`)
    - Filtrar produtos por atributo (`Categorias`)
    - Filtrar por preço
- Na coluna da direita:
    - Todos os produtos

### Página de recadinhos

Criar uma página em branco só com um bloco de texto de descrição dizendo que é a
página para as pessoas deixarem recados e habilitar os comentários nesta página.

### Configuração do form do carrinho

Por padrão o carrinho pede, mandatoriamente, os dados de endereço, por isso
usamos um plugin para editar esse form.

`Woocommerce` > `Checkout form`

Editar as opções do formulário desmarcando `required` e `enabled` nos campos não
desejados. (Basicamente manter Nome, sobrenome e email).

## Tema

O tema que utilizamos em nosso site (https://2021.baby.svabatone.family) foi o
[Babysitter Lite](https://wordpress.org/themes/babysitter-lite/), no qual
posteriormente fizemos algumas pequenas modificações.

### Estilo/Tema/CSS

As modificações no tema foram basicamente de "CSS" feitas par aa página de
"Recadinhos".  Elas foram feitas na área de "personalizações de CSS" do edito de
temas:

- https://<seu_site>/wp-admin/customize.php?autofocus%5Bsection%5D=custom_css

Estas foram as modificações que fizemos:

```
.camada {
	background: rgba(0,0,0,0.5);
  backdrop-filter: blur(1px);
}

ol.comment-list {
	display: grid;
	grid-template-columns: repeat(2, 1fr);
  gap: 10px;
}

ol.comment-list li.comment.byuser {
	margin: 5px;
	padding: 5px 20px;
	border-radius: 20px;
	background-color: rgba(255, 200, 80,0.2);
}

ol.comment-list li.comment.byuser article footer div.comment-metadata {
	display:inline-block;
}


ol.comment-list li.comment.byuser article footer div.comment-metadata, ol.comment-list li.comment.byuser article footer div.comment-metadata * {
  font: 12px 'Open Sans';
	color: #a5a5a5;
}

ol.comment-list li.comment.byuser footer .comment-metadata cite.fn {
	font: 16px/22px 'Open Sans';
	color: rgba(180, 70, 50, 1);
}

div#respond.comment-respond {
	display: block
	width: 100%;
}

div#respond.comment-respond textarea#comment {
	width: 100%;
}
```
